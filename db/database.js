const mongoose = require('mongoose');   


const mongodbConnection= async function(){

    try{
        const db =await mongoose.connect("mongodb://localhost:27017/OmegaPetShop");
        console.log('Database is conected ');
        }catch(error){
            console.log(error);
        }
    }

    module.exports = mongodbConnection;