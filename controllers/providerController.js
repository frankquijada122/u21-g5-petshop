
const Provider = require("../models/Provider");

const listProviders = async (req, res) => {
    try {
        const providers = await Provider.find();
        res.json(providers);
    } catch (error) {
        req.send(error);
    }
}
function findProvider( req, res ) {
    
    let id = req.params.id;
    let query = Provider.findById(id);

    query.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });

}

const addProvider = async (req, res) => {
    try{
        const newProvider = new Provider(req.body);
        const savedProvider = await newProvider.save();
        res.json(savedProvider);
    }catch(error){
        res.send(error);
    }
}

function updateProvider( req, res ){

    let id = req.params.id;
    let data = req.body;

    Provider.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );

}

function deleteProvider( req, res ){

    let id = req.params.id;

    Provider.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );
    
}


module.exports = {
    listProviders,
    addProvider,
    findProvider,
    updateProvider,
    deleteProvider
}