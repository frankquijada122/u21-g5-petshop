const User = require("../models/User");

const listUsers = async (req, res) => {
    try {
        const users = await User.find()//.populate('role');
        res.json(users);
    } catch (error) {
        req.send(error);
    }
}
function findUser( req, res ) {
    
    let id = req.params.id;
    let query = User.findById(id)//.populate('role');

    query.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });

}

const addUser = async (req, res) => {
    try{
        const newUser = new User(req.body);
        const savedUser = await newUser.save();
        res.json(savedUser);
    }catch(error){
        res.send(error);
    }
}

function updateUser( req, res ){

    let id = req.params.id;
    let data = req.body;

    User.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );

}

function deleteUser( req, res ){

    let id = req.params.id;

    User.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );
    
}


module.exports = {
    listUsers,
    addUser,
    findUser,
    updateUser,
    deleteUser
}