const Invoice = require("../models/Invoice");

const listInvoices = async (req, res) => {
    try {
        const invoices = await Invoice.find().populate('product').populate('user');
        res.json(invoices);
    } catch (error) {
        req.send(error);
    }
}
function findInvoice( req, res ) {
    
    let id = req.params.id;
    let query = Invoice.findById(id).populate('product').populate('user');

    query.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });

}

const addInvoice = async (req, res) => {
    try{
        const newInvoice = new Invoice(req.body);
        const savedInvoice = await newInvoice.save();
        res.json(savedInvoice);
    }catch(error){
        res.send(error);
    }
}

function updateInvoice( req, res ){

    let id = req.params.id;
    let data = req.body;

    Invoice.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );

}

function deleteInvoice( req, res ){

    let id = req.params.id;

    Invoice.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );
    
}


module.exports = {
    listInvoices,
    addInvoice,
    findInvoice,
    updateInvoice,
    deleteInvoice
}