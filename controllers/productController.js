const Product = require("../models/Product");

const listProducts = async (req, res) => {
    try {
        const products = await Product.find().populate('category').populate('provider');
        res.json(products);
    } catch (error) {
        req.send(error);
    }
}
function findProduct( req, res ) {
    
    let id = req.params.id;
    let query = Product.findById(id).populate('category').populate('provider');

    query.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });

}

const addProduct = async (req, res) => {
    try{
        const newProduct = new Product(req.body);
        const savedProduct = await newProduct.save();
        res.json(savedProduct);
    }catch(error){
        res.send(error);
    }
}

function updateProduct( req, res ){

    let id = req.params.id;
    let data = req.body;

    Product.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );

}

function deleteProduct( req, res ){

    let id = req.params.id;

    Product.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );
    
}


module.exports = {
    listProducts,
    addProduct,
    findProduct,
    updateProduct,
    deleteProduct
}