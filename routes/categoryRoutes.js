const express = require('express');


const {listCategories, addCategory,findCategory, updateCategory, deleteCategory} = require('../controllers/categoryController');

const router = express.Router();


router.get('/Categories/list',listCategories);
router.get('/Categories/:id',findCategory);
router.post('/Categories/add',addCategory);

router.put('/Categories/:id', updateCategory);
router.delete('/Categories/:id',deleteCategory );

module.exports = router;