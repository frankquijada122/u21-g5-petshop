const express = require('express');


const {listProviders, addProvider,findProvider, updateProvider, deleteProvider} = require('../controllers/providerController');

const router = express.Router();


router.get('/providers/list',listProviders);
router.get('/providers/:id',findProvider);
router.post('/providers/add',addProvider);

router.put('/providers/:id', updateProvider);
router.delete('/providers/:id',deleteProvider );

module.exports = router;