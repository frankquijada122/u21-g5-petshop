const express = require('express');


const {listUsers, addUser,findUser, updateUser, deleteUser} = require('../controllers/userController');

const router = express.Router();


router.get('/users/list',listUsers);
router.get('/users/:id',findUser);
router.post('/users/add',addUser);

router.put('/users/:id', updateUser);
router.delete('/users/:id',deleteUser );

module.exports = router;