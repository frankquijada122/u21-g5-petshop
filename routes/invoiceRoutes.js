const express = require('express');


const {listInvoices, addInvoice,findInvoice, updateInvoice, deleteInvoice} = require('../controllers/invoiceController');

const router = express.Router();


router.get('/invoice/list',listInvoices);
router.get('/invoice/:id',findInvoice);
router.post('/invoice/add',addInvoice);

router.put('/invoice/:id', updateInvoice);
router.delete('/invoice/:id',deleteInvoice );

module.exports = router;