const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    username: { type: String, unique: true, required: true},
    email: { type: String, unique: true, required: true},
    password: { type: String, required: true},
    adress: { type: String, required: true},
    city: { type: String, required: true},
    phone: { type: String, required: true},
    
    roles: [
        {
            type: mongoose.Types.ObjectId,
            ref: 'Rol',
        }
    ]
},
    {
        timestamps: true,
        versionKey: false,
    

 });
module.exports = mongoose.model('User',userSchema)