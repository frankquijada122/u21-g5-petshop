const mongoose = require('mongoose');

const invoiceSchema = new mongoose.Schema({
    date: { type: Date, default:Date.now()},
    
    product: [
        {
            ref: 'Product',
            type: mongoose.Types.ObjectId,
        }
    ],

    user: [
        {
            ref: 'User',
            type: mongoose.Types.ObjectId,
        }]
    ,



},
{
    timestamps: true,
    versionKey: false,
});


module.exports = mongoose.model('Invoice',invoiceSchema)